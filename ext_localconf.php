<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('komm_printbutton', '', '_pi1', '', TRUE);
$overrideSetup = 'plugin.tx_kommprintbutton_pi1.userFunc = CMC\PrintButton->main';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('tx_kommprintbutton', 'setup', $overrideSetup);