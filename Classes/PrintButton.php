<?php

namespace CMC;

/***************************************************************
*  Copyright notice
*
*  (c) 2009 <info@cmcitymedia.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

/**
 * Plugin 'Printbutton' for the 'komm_printbutton' extension.
 *
 * @author	cm city media <info@cmcitymedia.de>
 * @package	TYPO3
 * @subpackage	tx_kommprintbutton
 */
class PrintButton extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId      = 'tx_kommprintbutton_pi1';		// Same as class name
	var $scriptRelPath = 'Classes/PrintButton.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'komm_printbutton';	// The extension key.
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$urlParameters = array_unique(array_merge (\TYPO3\CMS\Core\Utility\GeneralUtility::_POST(), \TYPO3\CMS\Core\Utility\GeneralUtility::_GET()));
		$queryString = $this->parseArray($urlParameters);
		
		if (!$conf["windowWidth"]) {
			$conf["windowWidth"] = 600;
		}
		if (!$conf["windowHeight"]) {
			$conf["windowHeight"] = 600;
		}
		
		// eigene class für Printbutton, sonst leer
		if (!$conf["class"]) {
			$conf["class"] = '';
		}else{
			$conf["class"] = 'class="'.$conf["class"].'"';
		}
		
		if ($queryString) {
			$queryString = "&".$queryString;
		}
		$link = "window.location.origin + window.location.pathname + window.location.search + (window.location.search.length > 0 ? '&type=" . $conf['typeNum'] . "' : '?&type=" . $conf['typeNum'] . "')";
		$content  = '<a href="#" title="'.$conf['printtitle'].'" target="FEopenLink" '.$conf['class'].' onclick="vHWin=window.open('.$link.',\'FEopenLink\',\'width='.$conf["windowWidth"].',height='.$conf["windowHeight"].',scrollbars=yes\');vHWin.focus();return false;">'.$conf['label'].'</a>';
		return $content;
	}

	/**
	*	Parsed GET und POST Array zur Query-String
	*
	*	@access private
	*	@param array
	*	@param string
	*	@return string
	*/
	function parseArray($dataArray, $newKey='') {
		
		foreach ($dataArray as $key => $value) {
			if ('btnStart' == $key && $key != null){
				$value = ' |&lt; ';
			}
			if ('btnBack' == $key && $key != null){
				$value = ' &lt;&lt; ';
			}
			if ('btnNext' == $key && $key != null){
				$value = ' &gt;&gt; ';
			}
			if ('btnEnd' == $key && $key != null){
				$value = ' &gt;|';
			}
			if (is_array($value)) {
				if ($newKey) {
					$key = $newKey.'['.$key.']';
				} 
				$queryString .= $this->parseArray($value, $key);
			} else {
				if ($newKey) {
					$queryString .= $newKey.'['.strip_tags($key).']'.'='.strip_tags($value).'&';
				} else {
					if ($key != "id") {
						//fixes encoding curruption in backend editor
						//$queryString .= strip_tags($key).'='.strip_tags($value).'&';
					}
				}
			}
			
		}
		//return htmlspecialchars($queryString);
		return $queryString;
	}
}
